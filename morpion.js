class game {

    constructor() {
        this.i = 0;
        this.cases = document.querySelectorAll('.case')
        this.seconde = 0;
        this.minute = 0;
        this.timeOn = false;
        //variables pour le compteur
        this.moves = 0;
        this.counter = document.querySelector('.moves i');


    }

    playAgain(){
        this.cases.forEach(caseEl => caseEl.innerHTML = '');

        this.moves = 0;
    }

    player() {
        this.symbol = document.querySelector('#player');
        this.symbol.innerHTML = "X";

    }
    tour() {

        this.Time(this.seconde, this.minute);
        this.i++;

        if (!this.isGagner()) {
            if (this.i % 2 === 0) {
                this.symbol.innerHTML = "X"
                return 'O';
            }
            else {
                this.symbol.innerHTML = "O"
                return 'X';
            }
        }


        this.time(this.seconde, this.minute);
    }

    afficherGagnant() {
        if (this.symbol.innerHTML === 'O') {
            alert('X a gagné !')
        }
        else {
            alert('O a gagné !')
        }

        document.querySelector("#startAgainButton").style.display = "initial";
    }

    isGagner() {

        //lignes
        if (this.cases[0].innerHTML !== '' && this.cases[0].innerHTML === this.cases[1].innerHTML && this.cases[1].innerHTML === this.cases[2].innerHTML) {
            this.afficherGagnant(this.cases[0].innerHTML);
        }
        else if (this.cases[3].innerHTML !== '' && this.cases[3].innerHTML === this.cases[4].innerHTML && this.cases[4].innerHTML === this.cases[5].innerHTML) {
            this.afficherGagnant(this.cases[3].innerHTML);
        }

        else if (this.cases[6].innerHTML !== '' && this.cases[6].innerHTML === this.cases[7].innerHTML && this.cases[7].innerHTML === this.cases[8].innerHTML) {
            this.afficherGagnant(this.cases[6].innerHTML);
        }

        // colonnes
        else if (this.cases[0].innerHTML !== '' && this.cases[0].innerHTML === this.cases[3].innerHTML && this.cases[3].innerHTML === this.cases[6].innerHTML) {
            this.afficherGagnant(this.cases[0].innerHTML);
        }
        else if (this.cases[1].innerHTML !== '' && this.cases[1].innerHTML === this.cases[4].innerHTML && this.cases[4].innerHTML === this.cases[7].innerHTML) {
            this.afficherGagnant(this.cases[1].innerHTML);
        }
        else if (this.cases[2].innerHTML !== '' && this.cases[2].innerHTML === this.cases[5].innerHTML && this.cases[5].innerHTML === this.cases[8].innerHTML) {
            this.afficherGagnant(this.cases[2].innerHTML);
        }

        //DIAGONALES
        else if (this.cases[0].innerHTML !== '' && this.cases[0].innerHTML === this.cases[4].innerHTML && this.cases[4].innerHTML === this.cases[8].innerHTML) {
            this.afficherGagnant(this.cases[0].innerHTML);
        }
        else if (this.cases[2].innerHTML !== '' && this.cases[2].innerHTML === this.cases[4].innerHTML && this.cases[4].innerHTML === this.cases[6].innerHTML) {
            this.afficherGagnant(this.cases[2].innerHTML);
        }
    }

    jouer() {
        this.cases.forEach(caseEl => caseEl.addEventListener('click', event => {

            let clicked = event.target;

            if (caseEl.innerHTML === 'O' || caseEl.innerHTML === 'X') {
                return;
            } else {
                clicked.innerHTML = this.tour();
            }
            this.isGagner();
            this.addMove();
        }));

    }

    Time(seconde, minute) {
        if (this.timeOn == false) {
            this.timeOn = true;
            setInterval(function () {
                let TimeShow = document.querySelector('#Time');
                TimeShow.innerHTML = seconde;
                seconde++;
                if (seconde >= 40) {
                    minute++;
                    seconde = 0;
                }
                console.log(seconde);
            }, 1000);
        } else {
            return;
        }
    }
    addMove() {
        this.moves++;
        this.counter.innerHTML = this.moves;
    }
}

let play = new game;
play.jouer();
play.player();

